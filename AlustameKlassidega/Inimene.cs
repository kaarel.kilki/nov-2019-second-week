﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlustameKlassidega
{
    class Inimene   // klass on teisipidi andmetüüp
    {
        public string Nimi;
        public int? Vanus;
        public int KingaNumber;  //TODO: selle jaoks pean veel mingid reeglid paika panema

        public override string ToString()
        {
            return $"Inimene {Nimi} vanusega {(Vanus?.ToString() ?? "mida me ei tea")}";
        }


        // tegemist on meetodiga - teeb midagi (pole andmetüüpi (void)). Void tähendab mitte midagi, seetõttu ei nõua return'i.
        public void Trüki()
        {
            if (!Vanus.HasValue) return;

            Console.WriteLine(this);    // lause, mis täidetakse
        }


        // tegemist on funktsiooniga - arvutab midagi (on olemas andmetüüp (siin näites int?), mis vastusena tagastatakse)
        public int? OletatavSünniaasta()
        {
            return DateTime.Now.Year - Vanus;   // avaldis, mis tagastatakse
        }
    }
}
