﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// TODO: hiljem korjame siit ülearused read ära

namespace AlustameKlassidega
{
    class Program
    {
        static void Main(string[] args)
        {
            // alustaks nüüd progemisega

            //ÜtleTere();
            //ÜtleTere("Henn");
            //TervitPikalt("Henn", "Sarv");
            //ÜtleTere(5);

            Console.WriteLine("Kes sa oled: ");
            string kes = Console.ReadLine();
            Console.WriteLine($"Tere {ToProper(kes)}, meeldiv näha!");

            //Inimene mina = new Inimene();
            //mina.Nimi = "Henn";
            //mina.Vanus = 64;

            //Inimene tema = new Inimene();
            //tema.Nimi = "Raja Teele";
            

            //Inimene[] inimesed = new Inimene[10];
            //inimesed[0] = mina;
            //inimesed[1] = tema;

            ////Console.WriteLine(mina);
            ////Console.WriteLine(tema);

            //mina.Trüki();
            //tema.Trüki();

            //Console.WriteLine(mina.OletatavSünniaasta());

            //string tekstMuudetud;




        }

        static void ÜtleTere()
        {
            Console.WriteLine("Tere!");
        }

        static void ÜtleTere(string nimi)
        {
            Console.WriteLine($"Tere {nimi}!");
        }

        static void ÜtleTere(int mitu)
        {
            for (int i = 0; i < mitu; i++) Console.WriteLine($"Tere {i+1}. korda");
        }

        static void TervitPikalt(string eesNimi, string pereNimi)
        {
            Console.WriteLine($"Tere {eesNimi}!");
            Console.WriteLine($"Mul on hea meel näha kedagi perekonnast {pereNimi}!");
        }

        static string ToProperx(string tekst)
        {
            // 1 variant kuidas teha suure algustähega nimeks
            //string esitäht = tekst.Substring(0, 1);
            //string ülejäänud = tekst.Substring(1);

            //return esitäht.ToUpper() + ülejäänud.ToLower();


            // 2 variant kuidas ei löö ette errorit, kui jätta tühjaks
            return tekst == "" ? "Nimetu" : tekst.Substring(0, 1).ToUpper() + tekst.Substring(1).ToLower();
        }

        static string ToProper(string text)
        {
            string[] osad = text.Split(' ');
            for (int i = 0; i < osad.Length; i++)
            {
                osad[i] = ToProperx(osad[i]);
            }
            return string.Join(" ", osad);
        }


    }
}
