﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassJaStruct
{
    class Program
    {
        static void Main(string[] args)
        {
            // kui inimene on Class, siis mõlemad muutujad sisaldavad SAMA eksemplari
            InimeneClass esimeneC = new InimeneClass();     // classi puhul Inimene tehakse Heap'is ja struct puhul Stack'is
            esimeneC.Nimi = "Henn";
            esimeneC.Vanus = 64;
            InimeneClass teineC = esimeneC;
            teineC.Nimi = "Sarviktaat";
            Console.WriteLine(esimeneC);
            Console.WriteLine(teineC);

            // kui inimene on struct, siis iga muutujas on Oma eksemplar
            InimeneStruct esimeneS = new InimeneStruct();    // classi puhul Inimene tehakse Heap'is ja struct puhul Stack'is
            esimeneS.Nimi = "Henn";
            esimeneS.Vanus = 64;
            InimeneStruct teineS = esimeneS;
            teineS.Nimi = "Sarviktaat";
            Console.WriteLine(esimeneS);
            Console.WriteLine(teineS);

            // massiivid on alati classid
            int[] arvud = { 1, 2, 3, 4, 5 };
            int[] teised = (int[])arvud.Clone();
            teised[2] = 7;
            Console.WriteLine($"[{string.Join(",", arvud)}]");

        }
    }
    class InimeneClass
    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"Inimene (class) {Nimi} vanusega {Vanus}";
        
    }
    struct InimeneStruct
    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"Inimene (struct) {Nimi} vanusega {Vanus}";
    }
}
