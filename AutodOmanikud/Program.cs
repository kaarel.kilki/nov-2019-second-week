﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoOmanik
{
    static class E
    {
        public static string[] LoeFile(this string filename) => System.IO.File.ReadAllLines($"..\\..\\{filename}.txt");
        public static string[] Tükelda(this string rida) => rida.Split(',');

        public static string Küsi(this string küsimus)
        {
            Console.Write($"{küsimus}: ");
            return Console.ReadLine();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var autodF = "Autod".LoeFile();
            var juhidF = "Juhid".LoeFile();
            var omanikudF = "Omanikud".LoeFile();
            // vahva - kolm faili sisse loetud nagu niuhti

            Dictionary<string, Auto> autodD = new Dictionary<string, Auto>();
            Dictionary<string, Inimene> juhidD = new Dictionary<string, Inimene>();
            // vahva, nüüd on kus autosid ja inimesi pidada

            foreach (var x in autodF)
            {
                var xx = x.Tükelda();
                autodD.Add(xx[0], new Auto { NumbriMärk = xx[0], Mark = xx[1], Mudel = xx[2] });
            }
            foreach (var x in juhidF)
            {
                var xx = x.Tükelda();
                juhidD.Add(xx[0], new Inimene { IK = xx[0], Nimi = xx[1] });
            }
            // nii - autode ja inimestega on kutu - veel omanikud kirja panna
            foreach (var x in omanikudF)
            {
                var xx = x.Tükelda();
                juhidD[xx[0]].Autod.Add(autodD[xx[1]]);
                autodD[xx[1]].Omanik = juhidD[xx[0]];
            }
            // kui nüüd veel midagi vaja teha, siis teeb keegi teine
            var märk = "Anna numbrimärk".Küsi();
            Console.WriteLine(
            autodD.ContainsKey(märk) ? autodD[märk].Omanik?.Nimi ?? "omanik puudub" : "sellist autot pole"
            );

            var kood = "Anna isikukood".Küsi();
            if (juhidD.ContainsKey(kood))
            {
                if (juhidD[kood].Autod.Count > 0)
                    foreach (var aa in juhidD[kood].Autod) Console.WriteLine($"{aa.NumbriMärk} {aa.Mark} {aa.Mudel}");
                else Console.WriteLine("autosid tal ei ole");
            }
            else Console.WriteLine("tundmatu isik");

        }
    }

    class Inimene
    {
        public string IK { get; set; }
        public string Nimi { get; set; }

        public List<Auto> Autod { get; set; } = new List<Auto>();
    }

    class Auto
    {
        public string NumbriMärk { get; set; }
        public string Mark { get; set; }
        public string Mudel { get; set; }

        public Inimene Omanik { get; set; }
    }
}
