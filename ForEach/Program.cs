﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForEach
{
    static class E
    {
        // siia ma hakkan oma funtsioone ehitama
        public static IEnumerable<int> Paaris(this IEnumerable<int> mass)
        {
            foreach (var x in mass)
                if (x % 2 == 0) yield return x;
        }
        public static IEnumerable<int> Paaritu(this IEnumerable<int> mass)
        {
            foreach (var x in mass)
                if (x % 2 != 0) yield return x;
        }
        public static IEnumerable<int> Esimesed(this IEnumerable<int> mass, int mitu)
        {
            int mitmes = 0;
            foreach (var x in mass)
                if (mitmes++ < mitu) yield return x; else break;
        }
        public static IEnumerable<int> Millised(this IEnumerable<int> mass, Func<int,bool> f)
        {
            foreach (var x in mass) if (f(x)) yield return x;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            //Func<int, int> ruut = x => x * x;
            //Console.WriteLine(ruut(4));
            //Func<int, bool> kasSuur = x => x > 5;
            //if (kasSuur(ruut(4))) Console.WriteLine("on küll suur");

            // nüüd me õpime midagi ILUSAT!
            int[] arvud = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

            foreach (var x in arvud
                //.Paaritu()
                //.Esimesed(3)
                .Where(x => x % 2 == 0)
                //.Where(x => x < 10)
                //.Skip(3)
                //.Take(3)

                ) Console.WriteLine(x);

            Console.WriteLine(arvud.Where(x => x > 5).First());

            foreach (var x in arvud
                .Select(x => new { arv = x, ruut = x * x })
                .Where(x => x.ruut > 100)
                ) Console.WriteLine(x);

            var protok = System.IO.File.ReadAllLines("..\\..\\spordipäeva protokoll.txt")
                .Skip(1)
                .Select(x => x.Split(','))
                .Where(x => x.Length > 2)
                .Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2]) })
                .Select(x => new { x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants * 1.0 / x.Aeg })
                .ToList()
                ;

            foreach (var x in protok) Console.WriteLine(x);

            Console.WriteLine("kiireim oli: " +
            protok.OrderByDescending(x => x.Kiirus).First().Nimi
            );

            var q1 = protok.GroupBy(x => x.Distants)
                .Select(x => x.OrderByDescending(y => y.Kiirus).FirstOrDefault())
                .ToList();

            foreach (var x in q1) Console.WriteLine(x);
            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------------------------------");

            var q2 = protok.GroupBy(x => x.Nimi)
                .Select(x => x.Average(y => y.Aeg))
                .ToList()
                ;
            foreach (var x in q2) Console.WriteLine(x);

            var q3 = protok.GroupBy(x => x.Distants)
                .Select(x => x.OrderByDescending(y => y.Aeg).FirstOrDefault())
                .ToList();
            foreach (var x in q3) Console.WriteLine($"Kõige aeglasema oli {x.Nimi} ajaga {x.Aeg}");
            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------------------------------");

            // kõige stabiilsema jooksja leidmine
            Console.WriteLine(
            protok                          // protokoll
                .ToLookup(x => x.Nimi)      // portsionid nimede kaupa (koos portsioni nimega
                .Where(x => x.Count() > 1)  // jooksnud vähemalt 2 distantsi
                //.Select(x => new { Nimi = x.Key, Min = x.Min(y => y.Kiirus), Max = x.Max(y => y.Kiirus)})   // nimi, maksimaalne ja minimaalne kiirus
                .Select(x => new {Nimi = x.Key, Vahe = x.Max(y => y.Kiirus) - x.Min(y => y.Kiirus)})
                //.Select(x => new { x.Nimi, x.Min, x.Max, Vahe = x.Max-x.Min})   // nimi, vahe
                .OrderBy(x => x.Vahe)
                .Take(1)
                .Select(x => $"kõige stabiilsem oli {x.Nimi} kelle kiiruste vahe oli {x.Vahe}")
                .SingleOrDefault()
                );

            // iga distantsi ajavahe
            Console.WriteLine(
            protok
                .ToLookup(x => x.Distants)
                .Select(x => new { Distants = x.Key, Min = x.Min(y => y.Aeg), Max = x.Max(y => y.Aeg) })
                .Select(x => new { x.Distants, x.Min, x.Max, Vahe = x.Max - x.Min })
                .OrderBy(x => x.Vahe)
                .Take(1)
                .Select(x => $"Distantsi {x.Distants} meetrit kiireima ja aeglaseima aja vahe oli {x.Vahe}")
                .SingleOrDefault()
                );
            Console.WriteLine();
            var averag = Enumerable.Range(1, 10)
                .Select(x => (int?)x)       // muudab int-tüüpi int?-tüübiks(tal võib väärtus puududa)
                .Where(x => x > 10)
                .DefaultIfEmpty(null)
                .Average();                 // AverageOrDefault() C# ei ole
                ;
            Console.WriteLine(averag?.ToString()??"keskmist pole");
        }
    }
}
