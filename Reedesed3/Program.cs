﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Reedesed3
{
    class Program
    {
        static void Main(string[] args)
        {
            string peremees = ConfigurationManager.AppSettings["peremees"]; //"Henn";
            Console.WriteLine("Kes sa oled: ");
            string vastus = Console.ReadLine();
            if (vastus == peremees) Console.WriteLine("Tere peremees");
            else Console.WriteLine($"Tere {vastus}!");

            // siit jätkub tegevus
        }
    }

}
