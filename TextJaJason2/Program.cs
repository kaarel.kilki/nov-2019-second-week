﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace TextJaJason2
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "..\\..\\spordipäeva protokoll.txt";
            string filenameJson = "..\\..\\protokoll.json";

            if (false)
            {
                var protok = File.ReadAllLines(filename)
                    .Skip(1)
                    .Select(x => x.Split(','))
                    .Where(x => x.Length > 2)
                    .Select(x => new Tulemus { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2]) })
                    .ToList()
                    ;
                string json = JsonConvert.SerializeObject(protok);
                // Console.WriteLine(json);
                File.WriteAllText(filenameJson, json);
            }
            string loeJson = File.ReadAllText(filenameJson);
            var tulemused = JsonConvert.DeserializeObject<List<Tulemus>>(loeJson);
            foreach (var x in tulemused)
            {
                Console.WriteLine($"{x.Nimi} jooksis {x.Distants} ajaga {x.Aeg}.");
            }
            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine();

            string filename1 = "..\\..\\Õpetajad.csv";
            string filename1Json = "..\\..\\Õpetajad.json";

            if (true)
            {
                var õpet = File.ReadAllLines(filename1)
                        .Select(x => (x+",,,").Split(','))
                        .Select(x => new Õpetaja { IK = x[0].Trim(), Nimi = x[1].Trim(), Aine = x[2].Trim(), Klassijuhataja = x[3].Trim() })
                        .ToList()
                        ;
                string json1 = JsonConvert.SerializeObject(õpet);
                //Console.WriteLine(json);
                File.WriteAllText(filename1Json, json1);
            }

            string loeJson1 = File.ReadAllText(filename1Json);
            var õpetajad = JsonConvert.DeserializeObject<List<Õpetaja>>(loeJson1);
            foreach (var x in õpetajad)
            {
                Console.WriteLine($"Õpetaja{x.Nimi} isikukood on {x.IK} ja annab ainet {x.Aine} ja juhatab klassi {x.Klassijuhataja}.");
            }
            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine();

            string filename2 = "..\\..\\Õpilased.csv";
            string filename2Json = "..\\..\\Õpilased.json";

            if (false)      // false puhul ei kasutata loogeliste sulgude vahel olevat osa (kasutada kui ei viitsi kustutada) ja true puhul kasutatakse loogeliste sulgude vahelist osa
            {
                var õpil = File.ReadAllLines(filename2)
                    .Select(x => x.Split(','))
                    .Where(x => x.Length > 2)
                    .Select(x => new Õpilane { IK = x[0].Trim(), Nimi = x[1].Trim(), Klass = x[2].Trim() })
                    .ToList()
                    ;
                string json2 = JsonConvert.SerializeObject(õpil);
                File.WriteAllText(filename2Json, json2);
            }
            string loeJson2 = File.ReadAllText(filename2Json);
            var õpilased = JsonConvert.DeserializeObject<List<Õpilane>>(loeJson2);
            foreach (var x in õpilased)
            {
                Console.WriteLine($"Õpilane {x.Nimi} isikukoodiga {x.IK} käib {x.Klass} klassis.");
            }
            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine();

            string filename3 = "..\\..\\Hinded.txt";
            string filename3Json = "..\\..\\Hinded.json";

            if (true)
            {
                var hinne = File.ReadAllLines(filename3)
                        .Select(x => x.Split(','))
                        .Select(x => new Hinded { ÕpetajaIK = x[0].Trim(), ÕpilaseIK = x[1].Trim(), Aine = x[2].Trim(), Hinne = x[3].Trim() })
                        .ToList()
                        ;
                string json3 = JsonConvert.SerializeObject(hinne);
                //Console.WriteLine(json);
                File.WriteAllText(filename3Json, json3);
            }

            string loeJson3 = File.ReadAllText(filename3Json);
            var hinded = JsonConvert.DeserializeObject<List<Hinded>>(loeJson3);
            foreach (var x in hinded)
            {
                Console.WriteLine($"Õpetaja isikukoodiga {x.ÕpetajaIK} pani õpilasele isikukoodiga {x.ÕpilaseIK} aines {x.Aine} hinde {x.Hinne}.");
            }
        }
    }
    class Tulemus
    {
        public string Nimi { get; set; }
        public double Distants { get; set; }
        public double Aeg { get; set; }
        public double Kiirus {
            get => (double) Distants / Aeg; 
            set{}
        }
    }
    class Õpetaja
    {
        public string IK { get; set; }
        public string Nimi { get; set; }
        public string Aine { get; set; }
        public string Klassijuhataja { get; set; }
    }
    class Õpilane
    {
        public string IK { get; set; }
        public string Nimi { get; set; }
        public string Klass { get; set; }
    }
    class Hinded
    {
        public string ÕpetajaIK { get; set; }
        public string ÕpilaseIK { get; set; }
        public string Aine { get; set; }
        public string Hinne { get; set; }
    }
}
