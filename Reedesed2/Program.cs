﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Reedesed2
{
    class Program
    {
        static void Main(string[] args)
        {
            Debug.Listeners.Add(new TextWriterTraceListener(Console.Out));

            Console.WriteLine("Hello World!");
            Debug.WriteLine($"Alustame tööd kell {DateTime.Now.ToLongTimeString()}");
            Console.WriteLine("Teeme tööd");
            System.Threading.Thread.Sleep(5000);
            Console.WriteLine("Lõpetame töö");
            Debug.WriteLine($"Lõpetasime tööd kell {DateTime.Now.ToLongTimeString()}");
            Debug.Flush();
        }
    }
}
