﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom kroko = new Loom("krokodill");
            
            Loom ämmelgas = new Loom("ämblik");
            
            Loom elajas = new Loom();

            Kass k = new Kass ("Miisu","angoora");
 
            List<Loom> loomad = new List<Loom>
            {
                kroko, ämmelgas, elajas, k, new Koduloom("lammas", "Dolly")
            };

            foreach (var x in loomad) x.TeeHäält();
            Console.WriteLine("------------------------------");
            foreach (var x in loomad) Console.WriteLine(x);
            Console.WriteLine("------------------------------");
            foreach (var x in loomad) x.SikutaSabast();
            
            

            k.Silita();
            k.TeeHäält();
            k.SikutaSabast();
            k.TeeHäält();

            Koer koer = new Koer("Polla");
            Console.WriteLine(koer);
            koer.TeeHäält();
            Lõuna(new Sepik());
            Lõuna(koer);
            Lõuna(k);
            
        }
        public static void Lõuna(object x)
        {
            //if (x is ISöödav) ((ISöödav)x).Süüakse();
            //if (x is ISöödav xs) xs.Süüakse();            // is - true-false
            (x as ISöödav)?.Süüakse();                      // as (tyyp)objekt või null
        }
    }

    class Loom
    {
        public string Liik;
        public Loom(string liik) => Liik = liik;
        public Loom() : this("tundmatu liik") { }
        public virtual void TeeHäält() => Console.WriteLine($"{Liik} teeb koledat häält");
        public override string ToString() => $"loom liigist {Liik}";
        public void SikutaSabast() => Console.WriteLine($"HOIATUS! kui sikutada {Liik} looma sabast, siis see ei ole hea");
        

    }
    class Koduloom : Loom
    {
        public string Nimi;
        public Koduloom(string liik, string nimi) : base(liik) => Nimi = nimi;

        public override void TeeHäält() => Console.WriteLine($"{Nimi} möriseb mõnusasti");
        public override string ToString()
        {
            return "kodu"+base.ToString();
        }

    }
    class Kass : Koduloom
    {
        public string Tõug;
        public bool Tuju = false;
        public Kass(string nimi, string tõug) : base("Kass", nimi) => Tõug = tõug;
        public void Silita() => Tuju = true;
        public new void SikutaSabast() => Tuju = false;
        public override void TeeHäält()
        {
            if (Tuju) Console.WriteLine($"Kass {Nimi} lööb nurru");
            else Console.WriteLine($"{Nimi} kräunub");
        }
    }
    abstract class Ahaa
    {
        public abstract void Abstr();
        public virtual void Tore() => Console.WriteLine("on ju tore");

    }
    class Ehee : Ahaa
    {
        public override void Abstr()
        {
            throw new NotImplementedException();
        }
    }
    interface ISöödav
    {
        void Süüakse();
    }
    class Sepik : ISöödav
    {
        public void Süüakse()
        {
            Console.WriteLine("keegi nosib sepikut");
        }
    }
    class Koer : Koduloom, ISöödav
    {
        public string Tõug => "krants";
        public Koer(string nimi) : base("Koer", nimi) { }
        public override string ToString() => $"Koer {Nimi}";
        public override void TeeHäält() => Console.WriteLine($"{Nimi} haugub");
        public void Süüakse()
        {
            Console.WriteLine($"Koer {Nimi} pistetakse nahka");
        }        
    }
}
