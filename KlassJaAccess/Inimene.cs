﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassJaAccess
{   
    enum Gender { Female, Male}
    class Inimene
    {
        // static väljad ja propertyd
        public static int InimesteArv { get; private set; } = 0;                                                          // get_InimesteArv() fuktsioon
        public static List<Inimene> Inimesed = new List<Inimene>();

        // private väljad
        private int _InimeseNr = ++InimesteArv;
        private string _Nimi;
        
       
        //propertyd
        public string IK { get; }    // properti millel ei ole set osa nimetatakse read-only väljaga propertiks
        public string Nimi  // properti - omab get funtsiooni ja set meetodit
        {
            get => _Nimi;

            // set => _Nimi = Utils.ToProper(value); või
            set => _Nimi = value.ToProper();
        }
        

        // constructorid

        public Inimene(string IK)           //constructor - meetod klassi sees (ei oma void'i või mõmda muud asja), meetodi nimi langeb koku klassi nimega ja selle kutsub välja new operatsioone
        { 
            this.IK = IK;
            Inimesed.Add(this);
        }

        // construktoreid saab üksteise sappa kirjutada niimoodi ->
        public Inimene(string IK, string nimi) : this(IK) => _Nimi = nimi.ToProper();
                                                                   

        
        
        // arvutatavad propertyd
        public DateTime DateOfBirth => 
            new DateTime(
                    (((IK[0] - '1') / 2) + 18) * 100 +
                    int.Parse(IK.Substring(1, 2)),
                    int.Parse(IK.Substring(3, 2)),
                    int.Parse(IK.Substring(5, 2))
                    );
        
                #region get variandid
                // variant 1
                //int sajand = 19;
                //if (IK.Substring(0, 1) == "1" || IK.Substring(0, 1) == "2") sajand = 18;
                //else if (IK.Substring(0, 1) == "5" || IK.Substring(0, 1) == "6") sajand = 20;

                //// variant 2
                //switch(IK.Substring(0,1))
                //{
                //    case "1": case "2": sajand = 18; break;
                //    case "3": case "4": sajand = 19; break;
                //    case "5": case "6": sajand = 19; break;
                //}

                //// variant 3
                //sajand = ((_IK[0] - '1') / 2) + 18;
                #endregion


        public int Age => (DateTime.Today - DateOfBirth).Days * 4 / 1461;

        public Gender Gender => (Gender)(IK[0] % 2);

        //public string Gender
        //{
        //    get
        //    {
        //        string gender = "female";
        //        if (IK.Substring(0, 1) == "1" || IK.Substring(0, 1) == "3" || IK.Substring(0, 1) == "5") gender = "male";
        //        return gender;
        //    }
        //}


        //override'd
        public override string ToString() => $"{_InimeseNr}. Inimene {_Nimi} (IK={IK})";

        

    }
    static class Utils  // staatiline klass, mis sisaldab aid staatilisi asju
    {
        // näide extension funktsioonist, mis "laiendab" stringi klassi ja lubab seda funktsiooni käivitada kahte moodi:
        // Utils.ToProper(parameeter-string)
        // parameeter-string.ToProper
        public static string ToProperX(this string tekst) 
            => tekst == "" ? "" : tekst.Substring(0, 1).ToUpper() + tekst.Substring(1).ToLower();

        /*
        Tegi natuke ToProper() funktsiooni ringi. Esialgse variandi nimetas teise nimega, siis tal lihtsam. Tegi uue funktsiooni:
        1. Splitib teksti (enamasti nimi) tühikute kohalt - saab massiivi
        2. muudab selle massiivi elemendid (eelmist funktsiooni kasutades) suure algustähega
        3. Paneb selle saadud tulemuse string.Join abil uuesti kokku

        Lisaks (selle idee sai ühelt üpilaselt)
        ENNE splittimist asendab  kõik sidekriipsud sidepkriips ja tühikuga
        PEALE tagasi kokkupanemist asendab need "- " uuesti lihtsalt "-"
        */
        public static string ToProper(this string tekst)
            => string.Join(" ", tekst.Replace("-", "- ").Split(' ').Select(x => x.ToProperX())).Replace("- ", "-");
    }
}
