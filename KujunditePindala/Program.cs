﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KujunditePindala
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Kujund> kujundid = new List<Kujund>
            {
                new Ruut(),
            };
            foreach (var x in kujundid) Console.WriteLine(x);
        }
    }
    abstract class Kujund
    {
        double Pindala { get; }
        public Kujund(double pindala) => Pindala = pindala;
        public abstract void Abstr();
    }
    class Ruut: Kujund
    {
        double Serv;
        public Ruut(double pindala, double serv) : base(pindala) => Serv = serv;

        public override void Abstr()
        {
            double pindala = Serv * Serv;
        }
    }
    class Ristkülik: Kujund
    {
        double Kõrgus;
        double Laius;
        public Ristkülik(double pindala, double kõrgus) : base(pindala) => Kõrgus = kõrgus;
        public Ristkülik(double pindala, double laius)

        public override void Abstr()
        {
            double Pindala = Kõrgus * Laius;
        }
    }
    class Kolmnurk : Kujund
    {
        double Alus;
        double Kõrgus;

        public override void Abstr()
        {
            double Pindala = (Alus * Kõrgus) / 2;
        }
    }
    class Ring : Kujund
    {
        double Raadius;

        public override void Abstr()
        {
            double Pindala = Math.PI * (Raadius * Raadius);
        }
    }
}
