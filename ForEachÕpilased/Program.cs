﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ForEachÕpilased
{
    static class E
    {
        public static string[] LoeFile(this string filename) => File.ReadAllLines($"..\\..\\{filename}.txt");
    }
    class Program
    {
        static void Main(string[] args)
        {
            var õpilasedF = "Õpilased".LoeFile();
            var õpetajadF = "Õpetajad".LoeFile();
            var hindedF = "Hinded".LoeFile();
            var aineF = "KlassJaAine".LoeFile();

            // hiljem on vaja hindid ja õpilasi kokku panna koodi järgi, siis hea kui õpilased läheksid dictionary
            var Õpilased = õpilasedF
                .Select(x => x.Split(','))
                .Select(x => new { IK = x[0].Trim(), Nimi = x[1].Trim(), Klass = x[2].Trim() })
                .ToDictionary(x => x.IK)
                ;
            //kuna oleme juba ühe korra õpilased ssse lugenud ja dictionary pannud, siis ei ole vaja rohkem sisse lugeda, splittida ja trimmida
            Console.WriteLine("Õpilaste nimekiri klasside kaupa");
            var q1 = Õpilased.Values
//              .Select(x => x.Split(','))
//              .Select(x => new { IK = x[0].Trim(), Nimi = x[1].Trim(), Klass = x[2].Trim() })
                .GroupBy(x => x.Klass)
                ;
            foreach (var x in q1)
            {
                Console.WriteLine($"Klassi {x.Key} nimekiri");
                foreach (var n in x)
                    Console.WriteLine($"\t{n.Nimi} - {n.IK}");
            }
            
            Console.WriteLine();
            
            // Leian iga õpilase keskmise hinda
            Console.WriteLine("Iga õpilase keskmine hinne");
            var Hinded = hindedF
                .Select(x => x.Split(','))
                .Select(x => new { ÕpetajaIK = x[0].Trim(), ÕpilaseIK = x[1].Trim(), Aine = x[2].Trim(), Hinne = int.Parse(x[3])})

                ;
            foreach (var x in Hinded) Console.WriteLine($"Õpilane {(Õpilased.ContainsKey(x.ÕpilaseIK) ? Õpilased[x.ÕpilaseIK].Nimi : x.ÕpilaseIK)} sai aines {x.Aine} hindeks {x.Hinne}");

            var ÕpilasteKeskmised = Hinded
                .GroupBy(x => x.ÕpilaseIK)
                .Select(x => new { Nimi = Õpilased[x.Key].Nimi, Keskmine = x.Average(y => y.Hinne)})
                ;
            foreach (var v in ÕpilasteKeskmised) Console.WriteLine(v);
            Console.WriteLine();

            Console.WriteLine("Keskmine hinne ainete kaupa");
            
            Console.WriteLine();

            Console.WriteLine("Iga aine parim");
            var AineParim = Hinded
                .GroupBy(x => x.Aine)
                .Select(x => new { Aine = x.Key, Parim = x.OrderByDescending(y => y.Hinne).First()})
                .Select(x => new { x.Aine, Parim = Õpilased[x.Parim.ÕpilaseIK].Nimi, Hinne = x.Parim.Hinne})
                ;
            foreach (var v in AineParim) Console.WriteLine(v);

            var AineParimad = Hinded
                .GroupBy(x => x.Aine)
                .Select(x => new { Aine = x.Key, Parimad = x, ParimHinne = x.Max(y => y.Hinne) })
                .Select(x => new { x.Aine, Parimad = x.Parimad.Where(y => y.Hinne == x.ParimHinne)})
                ;
            foreach (var a in AineParimad)
            {
                Console.WriteLine("parimad õpilased aines:" + a.Aine);
                foreach (var v in a.Parimad)
                    Console.WriteLine($"\t{Õpilased[v.ÕpilaseIK].Nimi} hindega {v.Hinne}");

                
            }
            var aineteKlassideKaupa = Hinded
                .GroupBy(x => x.Aine)
                .Select(x => new { Aine = x.Key, HinneteKaupa = x.GroupBy(y => y.Hinne)})
                ;
            foreach (var a in aineteKlassideKaupa.OrderBy(x => x.Aine))
            {
                Console.WriteLine($"Aines {a.Aine} said õpilased selliseid hindeid:");
                foreach (var v in a.HinneteKaupa.OrderByDescending(x => x.Key))
                {
                    Console.WriteLine($"\tHinde {v.Key} said:");
                    foreach (var x in v)
                    {
                        Console.WriteLine("\t\t"+Õpilased[x.ÕpilaseIK].Nimi);
                    }
                }
            }
        }
    }
}
