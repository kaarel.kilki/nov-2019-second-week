﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeadProgrammis
{
    class Program
    {
        static void Main(string[] args)
        {
            try                      // proovime
            {
                Console.Write("Anna üks arv: ");
                int yks = int.Parse(Console.ReadLine());
                Console.Write("Anna teine arv: ");    
                int teine = int.Parse(Console.ReadLine());
                if (teine == 0) { throw new Exception("lõpeta see nulliga jagamine"); }
                Console.WriteLine($"{yks} jagatud {teine} on { yks / teine}");
            }
            catch (DivideByZeroException e)      // kui midagi juhtus
            {
                Console.WriteLine("jätame täna jagamata");
                
            }
            catch (FormatException e)
            {
                Console.WriteLine("õpi kirjutama");
            }
            catch
            {
                throw;
            }
            finally                 // täidetakse alati
            {
                Console.WriteLine("\nmina pesen oma käed puhtaks\n");
            }
            Console.WriteLine("seda vea korral ei tehta enam"); 
        }
    }
}
