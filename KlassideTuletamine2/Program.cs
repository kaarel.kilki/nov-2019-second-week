﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Inimene> nimekiri = new List<Inimene>
            {
                new Õpilane("Toomas Linnupoeg", "1A"),
                new Õpilane("Kiilike", "2C"),
                new Õpetaja("Henn Sarv", "matemaatika"),
                new Õpetaja("Malle Maasikas", "eesti keel"),
                new Inimene("direktor Kalju")
            };
            foreach (var x in nimekiri) Console.WriteLine(x);

            foreach (var x in nimekiri)
            {
                switch (x)
                {
                    case Õpilane õ:
                        Console.WriteLine($"{õ.Nimi} läheb kolhoosi porgandeid korjama");
                        break;
                    case Õpetaja õ:
                        Console.WriteLine($"{õ.Nimi} saab vaba päeva");
                        break;
                    default:
                        Console.WriteLine($"{x.Nimi} jääb kooli valvama");
                        break;
                }
                if (x is Õpilane jumbu) { Console.WriteLine($"{jumbu.Nimi} läheb kolhoosi porgandeid korjama nii nagu kogu {jumbu.Klass}"); }
                else if (x is Õpetaja õps) { Console.WriteLine($"{õps.Nimi} saab vaba päeva, et õppida aineks {õps.Aine}"); }
                else { Console.WriteLine($"{x.Nimi} jääb kooli valvama"); };
            }

            List<Kujund> kujundid = new List<Kujund>()
            {
                new Ring(10),
                new Ruut(10),
                new Kolmnurk(7, 4),
                new Ristkülik(10, 4)
            };
            foreach (var x in kujundid) Console.WriteLine($"{x} pindala: {x.Pindala:F2}");

            Console.WriteLine(kujundid.Sum(x => x.Pindala));

        }
    }
    class Inimene
    {
        public string Nimi;
        public Inimene(string nimi) => Nimi = nimi;
    }
    class Õpilane : Inimene
    {
        public string Klass;
        public Õpilane(string nimi, string klass) : base(nimi) => Klass = klass;
        public override string ToString() => $"õpilane {Nimi} klassist {Klass}";
    }
    class Õpetaja : Inimene
    {
        public string Aine;
        public Õpetaja(string nimi, string aine) : base(nimi) => Aine = aine;
        public override string ToString() => $"{Aine} õpetaja {Nimi}";
    }
    abstract class Kujund
    {
        public abstract double Pindala { get; }
    }
    class Ruut : Kujund
    {
        public double Serv { get; }
        public override double Pindala => Serv * Serv;
        public Ruut(double serv) => Serv = serv;
        public override string ToString() => $"Ruut {Serv}x{Serv}";
    }
    class Ristkülik : Kujund
    {
        public double Kõrgus { get; }
        public double Laius { get; }
        public override double Pindala => Kõrgus * Laius;
        public Ristkülik(double kõrgus, double laius) => (Kõrgus, Laius) = (kõrgus, laius);
        public override string ToString() => $"Ristkülik {Laius}x{Kõrgus}";
    }
    class Kolmnurk : Kujund
    {
        public double Kõrgus { get; }
        public double Alus { get; }
        public override double Pindala => Kõrgus * Alus / 2;
        public Kolmnurk(double kõrgus, double alus) => (Kõrgus, Alus) = (kõrgus, alus);
        public override string ToString() => $"Kolmnurk alus:{Alus} kõrgus:{Kõrgus}";
    }
    class Ring : Kujund
    {
        public double Raadius { get; }
        public override double Pindala => Raadius * Raadius * Math.PI;
        public Ring(double raadius) => Raadius = raadius;
        public override string ToString() => $"Ringi R: {Raadius}";
    }
}
