﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reedesed1
{
    class Program
    {
        static void Main(string[] args)
        {
            Tere();

            Console.WriteLine(Liida("+" ,new int[] { 1, 2, 3, 4, 5}));
            Console.WriteLine(Liida("*", 1,2,3));
            Console.WriteLine(Liida("+", 3));

            Console.WriteLine(Faktoriaal(5));
        }

        static void Tere(string sõna = "Tere", string nimi = "tundmatu") => Console.WriteLine($"{sõna} {nimi}!");
        //static void Tere() => Console.WriteLine($"Tere tundmatu!");

        //static int Liida(int a) => a;
        //static int Liida(int a, int b) => a + b;
        //static int Liida(int a, int b, int c) => Liida(a, Liida(b, c));
        static int Liida(string tehe = "+",params int[] arvud)
        {
            int summa = 1;
            foreach (var x in arvud) summa = tehe == "*" ? summa*x : summa+x ;
            return summa;
        }

        static int Faktoriaal(int x) => x < 2 ? x : x * Faktoriaal(x - 1);
    }
}
